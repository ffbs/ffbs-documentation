=======
 Build
=======

To build the documentation, execute:

.. code-block:: bash

   $ make html

and open the HTML documentation in build/html/index.html
